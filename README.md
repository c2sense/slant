# SLANT

## Overall description

The SLANT is an implementation of an SLA lifecycle manager system, compliant with WS-Agreement. This project is part of [c2sense](http://c2-sense.eu/).

The SLANT is a web, multi-platform application that allows to manage the whole lifecycle of service level agreements (from template creation to violation detection). It is a plugin-based decoupled component that can be adapted and exended to work on different platforms. It is an open source project, released under the Apache 2 license.

It is used in c2sense, in conjunction with the [SLANT GUI project](https://bitbucket.org/Lutech-C2SENSE/slant-gui) to allow to offer certain quality of service over the provided services, establishing service level agreements for them. It can operate at host level, virtual machine level and service level.

The SLANT provides mechanisms to support service level agreements management in individual and federated environments, based on [WS-Agreement specification](http://ogf.org/documents/GFD.192.pdf). 

The platform only pupports one-shot negotiation at the moment. The capabilities provided currently by the Lutech SLANT module are:

+ A language and a protocol to define and advertise the capabilities of service providers in SLA Template forms.
+ Creation agreements based on the templates.
+ Monitoring agreement compliance at runtime. An agreement between a service consumer and a service provider specifies one or more Service Level Objectives (SLOs). These are the expressions of the requirements of the service consumer and of the assurances by the service provider about the quality of services. An agreement lifecycle includes the creation, monitoring and termination of the agreement.

The WS-Agreement specification describes an XML schema for specifying service level agreements (both applicable to SLA Templates, Agreement Offers and Agreements). SLA Templates, Agreement Offers and Agreements are defined and described using the [WS-Agreement schema](http://schemas.ggf.org/graap/2007/03/ws-agreement).

##Features implemented

This version (v1) supports a language to express temporal monitoring restrictions. Thus, a service provider is able to define conditions like, for example, an availability of 99% calculated over 24 hours.

The SLANT supports this fature by adding a new data structure in the XML representation of the SLA Template/Agreement. For example, if a service provider wants to express that an availability of 99% of his service, calculated over 6 hours, the resulting template will include this in the ServiceLevelObjective section in the form of a CustomServiceLevel:

    <wsag:Template xmlns:wsag="http://www.ggf.org/namespaces/ws-agreement" xmlns:sla="http://slant.c2sense.eu" wsag:TemplateId="11c3e6d7-550c-4882-a419-09ece0c71d15">
        <wsag:Name>Template2</wsag:Name>
        <wsag:Context>
            <wsag:AgreementInitiator>Provider1</wsag:AgreementInitiator>
            <wsag:ServiceProvider>AgreementInitiator</wsag:ServiceProvider>
            <wsag:ExpirationTime>2018-03-05T12:00:00+0100</wsag:ExpirationTime>
            <sla:Service>Service1</sla:Service>
        </wsag:Context>
        <wsag:Terms>
            <wsag:All>
                <wsag:ServiceDescriptionTerm />
                <wsag:ServiceProperties wsag:ServiceName="Service1">
                    <wsag:VariableSet>
                        <wsag:Variable wsag:Name="sysUptime" />
                    </wsag:VariableSet>
                </wsag:ServiceProperties>
                <wsag:GuaranteeTerm wsag:Name="sysUptime">
                    <wsag:ServiceScope wsag:ServiceName="Service1" />
                    <wsag:ServiceLevelObjective>
                        <wsag:KPITarget>
                            <wsag:KPIName>sysUptime</wsag:KPIName>
                            <wsag:CustomServiceLevel>
                                {"policy": "(1 breach, 6 hours)", "constraint": "sysUptime LT 99"}
                            </wsag:CustomServiceLevel>
                        </wsag:KPITarget>
                    </wsag:ServiceLevelObjective>
                </wsag:GuaranteeTerm>
            </wsag:All>
        </wsag:Terms>
    </wsag:Template>

In addition, you can specify in the agreement, the parameters necessary to the monitoring service without having to notify them later to the system administrator:

    <wsag:Agreement xmlns:wsag="http://www.ggf.org/namespaces/ws-agreement" TemplateId="11c3e6d7-550c-4882-a419-09ece0c71d15">
        ...
        <wsag:Terms>
            <wsag:All>
                ...
                <wsag:ServiceReference
                        wsag:Name="CoordsRequest"
                        wsag:ServiceName="GPSService0001">
                    <wsag:ServiceVariableSet>
                        <wsag:ServiceVariable wsag:Name="URL_Monitor">
                            <wsag:Value>http://somehost:someport/C2SenseSampleService/rest/monitoring</wsag:Value>
                        </wsag:ServiceVariable>
                    </wsag:ServiceVariableSet>
                </wsag:ServiceReference>
                ...
            </wsag:All>
        </wsag:Terms>
    </wsag:Template>

## Installation Guide ##

Read the [Installation Guide][1].

## User Guide ##

Read the [User Guide][2].

## WS-Agreement ##

Read the [WS-Agreement][3].

## Developer Guide ##

Read the [Developer Guide][4].

##License##

Licensed under the [Apache License, Version 2.0][8]

[1]: docs/installation-guide.md
[2]: docs/user-guide.md
[3]: docs/ws-agreement.md
[4]: docs/developer-guide.md
[8]: http://www.apache.org/licenses/LICENSE-2.0