package eu.atos.sla.datamodel.bean;

import eu.atos.sla.datamodel.*;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * A POJO Object that stores all the information from a Agreement
 * 
 * @author Pedro Rey - Atos
 */
@Entity
@Table(name = "agreement")
@NamedQueries({
		@NamedQuery(name = Agreement.QUERY_FIND_ALL, query = "SELECT p FROM Agreement p"),
		@NamedQuery(name = Agreement.QUERY_FIND_BY_AGREEMENT_ID, query = "SELECT p FROM Agreement p where p.agreementId = :agreementId "),
		@NamedQuery(name = Agreement.QUERY_FIND_BY_CONSUMER, query = "SELECT p FROM Agreement p where p.consumer = :consumerId "),
		@NamedQuery(name = Agreement.QUERY_FIND_BY_PROVIDER, query = "SELECT p FROM Agreement p where  p.provider.uuid = :providerUuid "),
		@NamedQuery(name = Agreement.QUERY_FIND_BY_TEMPLATEUUID, query = "SELECT p FROM Agreement p where  p.template.uuid = :templateUUID "),
		@NamedQuery(name = Agreement.QUERY_ACTIVE_AGREEMENTS, query = "SELECT p FROM Agreement p where p.expirationDate > :actualDate "),
		@NamedQuery(name = Agreement.QUERY_FIND_BY_TEMPLATEUUID_AND_CONSUMER, query = "SELECT p FROM Agreement p where (p.template.uuid = :templateUUID) AND (p.consumer = :consumerId)"),
		@NamedQuery(name = Agreement.QUERY_SEARCH, query = "SELECT a FROM Agreement a "
				+ "WHERE (:providerId is null or a.provider.uuid = :providerId) "
				+ "AND (:consumerId is null or a.consumer = :consumerId) "
				+ "AND (:templateId is null or a.template.uuid = :templateId) "
				+ "AND (:active is null "
				+ "    or (:active = true and a.expirationDate > current_timestamp()) "
				+ "    or (:active = false and a.expirationDate <= current_timestamp()))") })

public class Agreement implements IAgreement, Serializable {
	public final static String QUERY_FIND_ALL = "Agreement.findAll";
	public final static String QUERY_FIND_ALL_AGREEMENTS = "Agreement.findAllAgreements";
	public final static String QUERY_FIND_BY_PROVIDER = "Agreement.findByProvider";
	public final static String QUERY_FIND_BY_CONSUMER = "Agreement.findByConsumer";
	public final static String QUERY_FIND_BY_AGREEMENT_ID = "Agreement.getByAgreementId";
	public final static String QUERY_ACTIVE_AGREEMENTS = "Agreement.getActiveAgreements";
	public final static String QUERY_FIND_BY_TEMPLATEUUID = "Agreement.getByTemplateUUID";
	public final static String QUERY_FIND_BY_TEMPLATEUUID_AND_CONSUMER = "Agreement.getByTemplateUUIDAndConsumer";
	public final static String QUERY_SEARCH = "Agreement.search";

	private static final long serialVersionUID = -5939038640423447257L;

	private Long id;
	private String agreementId;
	private String consumer;
	private IProvider provider;
	private ITemplate template;
	private Date expirationDate;
	private AgreementStatus status;
	private String text;
	private List<IServiceReference> serviceReference;
	private List<IServiceProperties> serviceProperties;
	private List<IGuaranteeTerm> guaranteeTerms;
	private String serviceId;
	private Boolean hasGTermToBeEvaluatedAtEndOfEnformcement;
	private String name;
							
	public Agreement() {
	}

	public Agreement(String agreementId) {
		this.agreementId = agreementId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "agreement_id", unique = true)
	public String getAgreementId() {

		return this.agreementId;
	}

	public void setAgreementId(String agreementId) {

		this.agreementId = agreementId;
	}

	@Column(name = "consumer")
	public String getConsumer() {
		return this.consumer;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}

	@ManyToOne(targetEntity = Provider.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "provider_id", referencedColumnName = "id", nullable = false)
	public IProvider getProvider() {
		return this.provider;
	}

	public void setProvider(IProvider provider) {
		this.provider = provider;
	}

	@ManyToOne(targetEntity = Template.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "template_id", referencedColumnName = "id", nullable = false)
	public ITemplate getTemplate() {
		return this.template;
	}

	public void setTemplate(ITemplate template) {
		this.template = template;
	}

	@Column(name = "expiration_time")
	public Date getExpirationDate() {
		return this.expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Transient
	public AgreementStatus getStatus() {
		return this.status;
	}

	public void setStatus(AgreementStatus status) {
		this.status = status;
	}

	@Column(name = "text", columnDefinition = "longtext")
	@Lob
	public String getText() {
		return this.text;
	}

	@Lob
	public void setText(String text) {
		this.text = text;
	}

	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	@OneToMany(targetEntity = ServiceReference.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "agreement_id", referencedColumnName = "agreement_id", nullable = true)
	public List<IServiceReference> getServiceReference() {
		return this.serviceReference;
	}

	public void setServiceReference(List<IServiceReference> serviceReference) {
		this.serviceReference = serviceReference;
	}

	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	@OneToMany(targetEntity = ServiceProperties.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "agreement_id", referencedColumnName = "agreement_id", nullable = true)
	public List<IServiceProperties> getServiceProperties() {
		return this.serviceProperties;
	}

	public void setServiceProperties(List<IServiceProperties> serviceProperties) {
		this.serviceProperties = serviceProperties;
	}

	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	@OneToMany(targetEntity = GuaranteeTerm.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "agreement_id", referencedColumnName = "agreement_id", nullable = true)
	public List<IGuaranteeTerm> getGuaranteeTerms() {
		return this.guaranteeTerms;
	}

	public void setGuaranteeTerms(List<IGuaranteeTerm> guaranteeTerms) {
		this.guaranteeTerms = guaranteeTerms;
	}

	@Column(name = "service_id")
	public String getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@Column(name = "metrics_eval_end")
	public Boolean getHasGTermToBeEvaluatedAtEndOfEnformcement() {
		return this.hasGTermToBeEvaluatedAtEndOfEnformcement;
	}

	public void setHasGTermToBeEvaluatedAtEndOfEnformcement(
			Boolean hasGTermToBeEvaluatedAtEndOfEnformcement) {
		this.hasGTermToBeEvaluatedAtEndOfEnformcement = hasGTermToBeEvaluatedAtEndOfEnformcement;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}