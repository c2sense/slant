package eu.atos.sla.dao.jpa;

import eu.atos.sla.dao.IServiceReferenceDAO;
import eu.atos.sla.datamodel.IServiceReference;
import eu.atos.sla.datamodel.bean.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository("ServiceReferenceRepository")
public class ServiceReferenceDAOJpa implements IServiceReferenceDAO {
	private static Logger logger = LoggerFactory.getLogger(ServiceReferenceDAOJpa.class);
	private EntityManager entityManager;

	@PersistenceContext(unitName = "slantrepositoryDB")
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	@Override
	public IServiceReference getByAgreementId(String agreementId) {
		try {
			TypedQuery<IServiceReference> query = this.entityManager.createNamedQuery(
					ServiceReference.QUERY_FIND_BY_AGREEMENT_ID, IServiceReference.class);
			query.setParameter("agreementId", agreementId);
			IServiceReference serviceReference = query.getSingleResult();
			logger.debug("ServiceReference found; [name, servicename] [: " + serviceReference.getName() + ", " + serviceReference.getServiceName() + "]");

			return serviceReference;

		} catch (NoResultException e) {
			logger.debug("No Result found: " + e);
			return null;
		}
	}
}
