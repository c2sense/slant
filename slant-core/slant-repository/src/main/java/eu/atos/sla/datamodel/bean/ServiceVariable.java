package eu.atos.sla.datamodel.bean;

import eu.atos.sla.datamodel.IServiceVariable;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "service_variable")
@NamedQueries({
		@NamedQuery(name = ServiceVariable.QUERY_FIND_ALL, query = "SELECT p FROM ServiceVariable p"),
		@NamedQuery(name = ServiceVariable.QUERY_FIND_BY_NAME, query = "SELECT p FROM ServiceVariable p where p.name = :name") })
public class ServiceVariable implements IServiceVariable, Serializable {

	public final static String QUERY_FIND_ALL = "ServiceVariable.findAll";
	public final static String QUERY_FIND_BY_NAME = "ServiceVariable.findByName";

	private static final long serialVersionUID = 36344868689340923L;
	private Long id;
	private String name;
	private String value;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "value")
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
