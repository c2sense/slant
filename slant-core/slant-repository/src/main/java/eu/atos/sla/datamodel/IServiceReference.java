package eu.atos.sla.datamodel;

import java.util.List;

public interface IServiceReference {

	String getName();

	void setName(String name);

	String getServiceName();

	void setServiceName(String serviceName);

	List<IServiceVariable> getServiceVariableSet();

	void setServiceVariableSet(List<IServiceVariable> serviceVariableSet);

}
