package eu.atos.sla.datamodel.bean;

import eu.atos.sla.datamodel.IServiceReference;
import eu.atos.sla.datamodel.IServiceVariable;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "service_reference")
@NamedNativeQueries({
		@NamedNativeQuery(name = ServiceReference.QUERY_FIND_BY_AGREEMENT_ID, query = "SELECT * FROM service_reference sr WHERE sr.agreement_id= :agreementId" ,
				resultClass = ServiceReference.class)
})
@NamedQueries({
		@NamedQuery(name = ServiceReference.QUERY_FIND_ALL, query = "SELECT p FROM ServiceReference p")
})
public class ServiceReference implements IServiceReference, Serializable {

	public final static String QUERY_FIND_ALL = "ServiceReference.findAll";
	public final static String QUERY_FIND_BY_AGREEMENT_ID = "ServiceReference.findByAgreementId";

	private static final long serialVersionUID = 8160422880355293305L;
	private Long id;
	private String name;
	private String serviceName;

	private List<IServiceVariable> serviceVariableSet;

	public ServiceReference() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "name", nullable = true)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "service_name")
	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	
	@Fetch(org.hibernate.annotations.FetchMode.SUBSELECT)
	@OneToMany(targetEntity = ServiceVariable.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "service_reference_id", referencedColumnName = "id", nullable = true)
	public List<IServiceVariable> getServiceVariableSet() {
		return this.serviceVariableSet;
	}

	public void setServiceVariableSet(List<IServiceVariable> variableSet) {
		this.serviceVariableSet = variableSet;
	}

}
