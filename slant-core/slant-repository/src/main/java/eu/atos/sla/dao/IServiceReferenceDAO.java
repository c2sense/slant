package eu.atos.sla.dao;

import eu.atos.sla.datamodel.IServiceReference;

/**
 * DAO interface to access to the ServiceReference information
 * 
 * @author Massimo Sorbara - Lutech
 * 
 */
public interface IServiceReferenceDAO {

	/**
	 * Returns the ServiceReference from the database by its agreement id
	 * 
	 * @param agreementId
	 *            of the ServiceReference
	 * @return the corresponding ServiceReference from the database
	 */
	IServiceReference getByAgreementId(String agreementId);

}
