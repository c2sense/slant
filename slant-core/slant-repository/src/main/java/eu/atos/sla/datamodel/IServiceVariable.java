package eu.atos.sla.datamodel;



public interface IServiceVariable {

	
	/*
	 * Internal generated ID
	 */
	Long getId();
	void setId(Long id);
	
	String getName();

	void setName(String name);

	String getValue();

	void setValue(String value);

}
