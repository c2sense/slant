package eu.atos.sla.enforcement;

import eu.atos.sla.dao.*;
import eu.atos.sla.datamodel.*;
import eu.atos.sla.datamodel.ICompensation.IPenalty;
import eu.atos.sla.datamodel.ICompensation.IReward;
import eu.atos.sla.datamodel.IGuaranteeTerm.GuaranteeTermStatusEnum;
import eu.atos.sla.datamodel.bean.Agreement;
import eu.atos.sla.datamodel.bean.EnforcementJob;
import eu.atos.sla.evaluation.guarantee.GuaranteeTermEvaluator.GuaranteeTermEvaluationResult;
import eu.atos.sla.monitoring.IMonitoringMetric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 
 * @author rsosa
 *
 */
@Service("EnforcementManager")
@Transactional
public class EnforcementService implements IEnforcementService {
	private static Logger logger = LoggerFactory.getLogger(EnforcementService.class);
	
	@Autowired
	IEnforcementJobDAO enforcementJobDAO;

	@Autowired
	IAgreementDAO agreementDAO;
	
	@Autowired
	IGuaranteeTermDAO guaranteeTermDAO;
	
	@Autowired
	IViolationDAO violationDAO;

	@Autowired
	IPenaltyDAO penaltyDAO;
	
	@Autowired
	AgreementEnforcement agreementEnforcement;
	
	
	@Override
	public IEnforcementJob getEnforcementJob(Long id) {

		return this.enforcementJobDAO.getById(id);
	}

	@Override
	public IEnforcementJob getEnforcementJobByAgreementId(String agreementId) {

		return this.enforcementJobDAO.getByAgreementId(agreementId);
	}

	@Override
	public List<IEnforcementJob> getEnforcementJobs() {

		return this.enforcementJobDAO.getAll();
	}

	@Override
	public IEnforcementJob createEnforcementJob(IEnforcementJob job) {

		String agreementId = job.getAgreement().getAgreementId();
		if (this.enforcementJobDAO.getByAgreementId(agreementId) != null) {
			throw new IllegalStateException("Agreement " + agreementId
					+ " already has EnforcementJob");
		}
		IAgreement agreement = this.agreementDAO.getByAgreementId(agreementId);
		if (agreement==null)
			throw new IllegalStateException("Agreement " + agreementId
					+ " doesn't exist in the database, cannot be associated to the enforcement");

		job.setAgreement(agreement);

		IEnforcementJob saved = this.enforcementJobDAO.save(job);

		return saved;

	}
	
	@Override
	public IEnforcementJob createEnforcementJob(String agreementId) {

		IEnforcementJob job = new EnforcementJob();
		IAgreement agreement = new Agreement();
		agreement.setAgreementId(agreementId);
		job.setAgreement(agreement);
		job.setEnabled(false);
		
		IEnforcementJob saved = createEnforcementJob(job);
		return saved;
	}

	@Override
	public boolean deleteEnforcementJobByAgreementId(String agreementId) {
		IEnforcementJob job = this.enforcementJobDAO.getByAgreementId(agreementId);

		if (job == null) {
			return false;
		}

		boolean result = this.enforcementJobDAO.delete(job);

		return result;
	}

	@Override
	public boolean startEnforcement(String agreementId) {
		IEnforcementJob job = this.enforcementJobDAO.getByAgreementId(agreementId);

		if (job == null) {
			return false;
		}
		job.setEnabled(true);
		this.enforcementJobDAO.save(job);
		
		return true;
	}

	@Override
	public boolean stopEnforcement(String agreementId) {
		IEnforcementJob job = this.enforcementJobDAO.getByAgreementId(agreementId);
		if (job == null) {
			return false;
		}
		job.setEnabled(false);
		this.enforcementJobDAO.save(job);
			IAgreement agreement = this.agreementDAO.getByAgreementId(agreementId);
			if (agreement.getHasGTermToBeEvaluatedAtEndOfEnformcement()!= null){
				if (agreement.getHasGTermToBeEvaluatedAtEndOfEnformcement()){
					try{
						this.agreementEnforcement.enforce(agreement, job.getLastExecuted(), true);
					}catch(Throwable t){
						logger.error("Fatal error in stopEnforcement doing enforcement", t);
					}

				}
			}
		return true;
	}

	@Override
	public void saveEnforcementResult(IAgreement agreement,
			Map<IGuaranteeTerm, GuaranteeTermEvaluationResult> enforcementResult) {
		
		for (IGuaranteeTerm gterm : enforcementResult.keySet()) {
			IGuaranteeTerm dbTerm = this.guaranteeTermDAO.getById(gterm.getId());
			GuaranteeTermEvaluationResult gttermResult = enforcementResult.get(gterm);
			
			for (IViolation violation : gttermResult.getViolations()) {
				dbTerm.getViolations().add(violation);
				this.violationDAO.save(violation);
			}
			
			for (ICompensation compensation : gttermResult.getCompensations()) {
				
				if (compensation instanceof IPenalty) {
					IPenalty penalty = (IPenalty)compensation;
					this.penaltyDAO.save(penalty);
					dbTerm.getPenalties().add(penalty);
				}
				else if (compensation instanceof IReward) {
					logger.warn("Saving a Reward is not implemented");
				}
				else {
					throw new AssertionError("Unexpected compensation type: " + compensation.getClass().getName());
				}
			}
			
			dbTerm.setStatus( dbTerm.getViolations().size() > 0? 
					GuaranteeTermStatusEnum.VIOLATED : GuaranteeTermStatusEnum.FULFILLED);
			try{
				this.guaranteeTermDAO.update(dbTerm);
			}catch(Exception e){
				// sometimes the update failes, for example in the test cases.
				//in such a case 
				this.guaranteeTermDAO.save(dbTerm);
			}
		}
		
		IEnforcementJob job = getEnforcementJobByAgreementId(agreement.getAgreementId());
		job.setLastExecuted(new Date());
		if (job.getFirstExecuted() == null) job.setFirstExecuted(job.getLastExecuted());
		this.enforcementJobDAO.save(job);
		logger.info("saved enforcement result(agreement=" + agreement.getAgreementId()+")");
	}
	
	public void enforceReceivedMetrics(
			IAgreement agreement, String guaranteeTermName, List<IMonitoringMetric> metrics) {
		
		logger.debug(
				"enforceReceivedMetrics(agreement=" + agreement.getAgreementId() + ", gt=" + guaranteeTermName + ")");
		Map<IGuaranteeTerm, List<IMonitoringMetric>> metricsMap = 
				new HashMap<IGuaranteeTerm, List<IMonitoringMetric>>();

		for (IGuaranteeTerm gt : agreement.getGuaranteeTerms()) {
			if (guaranteeTermName.equals(gt.getName())) {
				metricsMap.put(gt, metrics);
			}
			else {
				metricsMap.put(gt, Collections.<IMonitoringMetric>emptyList());
			}
		}
		this.agreementEnforcement.enforce(agreement, metricsMap);
	}
	
	@Override
	public void doEnforcement(IAgreement agreement,
			Map<IGuaranteeTerm, List<IMonitoringMetric>> metrics) {

		logger.debug("enforceReceivedMetrics(" + agreement.getAgreementId() + ")");
		this.agreementEnforcement.enforce(agreement, metrics);
	}

	

	@Override
	public void saveCheckedGuaranteeTerm(IGuaranteeTerm term) {
		term.setLastSampledDate(new Date());
		this.guaranteeTermDAO.update(term);
	}
}
