package eu.atos.sla.enforcement.schedule;

import eu.atos.sla.dao.IAgreementDAO;
import eu.atos.sla.dao.IEnforcementJobDAO;
import eu.atos.sla.datamodel.IEnforcementJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * This class is the root of the execution of the enforcement tasks.
 * 
 * When in a spring context, is executed periodically according to the value of <code>spawnlookup.cron</code>.
 * 
 * Once running, it finds the enforcement jobs to run since some date (<code>poll.interval</code>
 * dependent) and schedules a task to check each agreement.
 * 
 * 
 * Following properties must have been set before running in production mode
 * 
 * it.lutech.c2sense.slant.enforcement.spawnlookup.cron=*\/5 * * * * *
 * it.lutech.c2sense.slant.enforcement.poll.interval.mseconds=30000
 */
@Component
@Transactional
public class ScheduledEnforcementWorker implements InitializingBean, IScheduledEnforcementWorker {
	private static final String POLL_INTERVAL = "it.lutech.c2sense.slant.enforcement.poll.interval.mseconds";

	private static final String CRON = "it.lutech.c2sense.slant.enforcement.spawnlookup.cron";

	private static Logger logger = LoggerFactory.getLogger(ScheduledEnforcementWorker.class);

	@Value("ENF{" + CRON + "}")
	private String cron;
	
	@Value("ENF{" + POLL_INTERVAL + "}")
	private String pollIntervalString;
	private long pollInterval;
	
	@Autowired
	private IEnforcementJobDAO enforcementJobDAO;

	@Autowired
	private ApplicationContext applicationContext;


	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Autowired
	private IAgreementDAO agreementDAO;

	/* (non-Javadoc)
	 * @see eu.atos.sla.enforcement.IScheduledEnforcementWorker#spawnMonitors()
	 */
	
	
	@Override
	@Scheduled(cron = "ENF{" + CRON + "}")
	public void spawnMonitors() {
		Date since = computeOffset();
		List<IEnforcementJob> nonExecuted = this.enforcementJobDAO.getNotExecuted(since);
		logger.debug("spawning {} tasks", nonExecuted.size());
		for (IEnforcementJob job : nonExecuted) {
			try{
				EnforcementTask et = new EnforcementTask(job);
				this.applicationContext.getAutowireCapableBeanFactory().autowireBean(et);
				this.taskExecutor.execute(et);
			}catch(Throwable t){
				logger.error("Error while executing enforcement job",t);				
			}
		}
	}
	
	
	private Date computeOffset(){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MILLISECOND, -((int)(this.pollInterval)));
		return calendar.getTime();
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		try {
			this.pollInterval = Long.parseLong(this.pollIntervalString);
		}catch(NumberFormatException npe){
			String str = String.format("Can not parse ENF{%s} value{%s}. Is it a number?", 
					POLL_INTERVAL, this.pollIntervalString);
			throw new IllegalArgumentException(str);
		}

		logger.debug("EnforcementWorker registered, cron[{}], interval[{}]",
				this.cron, this.pollIntervalString);
	}
	

}
