/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.sla.notification;

import eu.atos.sla.datamodel.IAgreement;
import eu.atos.sla.datamodel.IGuaranteeTerm;
import eu.atos.sla.evaluation.guarantee.GuaranteeTermEvaluator;
import eu.atos.sla.notification.IAgreementEnforcementNotifier;
import it.lutech.c2sense.sla.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Created by massimo on 15/03/17.
 */
public class EmailEnforcementNotifier implements IAgreementEnforcementNotifier {
    private static Logger logger = LoggerFactory.getLogger(EmailEnforcementNotifier.class);

    @Autowired
    private MailService mailService;

    @Override
    public void onFinishEvaluation(IAgreement agreement,
                                   Map<IGuaranteeTerm, GuaranteeTermEvaluator.GuaranteeTermEvaluationResult> guaranteeTermEvaluationMap) {
        StringBuilder sb = new StringBuilder(5);
        logger.debug("Notifying onFinishEvaluation {}", agreement.getId());
        sb.append("\n==========================");
        sb.append("\nAgreement Id: ").append(agreement.getId());
        sb.append("\nAgreement Name: ").append(agreement.getName());
        sb.append("\nAgreement ExpirationDate: ").append(agreement.getExpirationDate());
        sb.append("\nAgreement Provider Name: ").append(agreement.getProvider().getName());
        sb.append("\n==========================");
        for (Map.Entry<IGuaranteeTerm, GuaranteeTermEvaluator.GuaranteeTermEvaluationResult> e:guaranteeTermEvaluationMap.entrySet()) {
            IGuaranteeTerm gt = e.getKey();
            GuaranteeTermEvaluator.GuaranteeTermEvaluationResult gtresult = e.getValue();
            logger.debug("Notifying onFinishEvaluation GuaranteeTerm:{} Num violations:{} Num compensations:{} ", gt.getId(), gtresult.getViolations().size(), gtresult.getCompensations().size());
            sb.append("\nNotifying onFinishEvaluation GuaranteeTerm:").append(gt.getId()).append(";\nNum violations: ").append(gtresult.getViolations().size()).append(";\nNum compensations: ").append(gtresult.getCompensations().size()).append(";");
        }
        logger.debug(" onFinishEvaluation", agreement.getId());
        sb.append("\nonFinishEvaluation").append(agreement.getId());

        this.mailService.sendMail("m.sorbara@lutech.it", null, "Enforcement on Id Agreement " + agreement.getId(), sb.toString());

    }

}
