/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.sla.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;

/**
 * Created by massimo on 16/03/17.
 */
public class MailService {

    private static Logger logger = LoggerFactory.getLogger(MailService.class);

    private String mailServiceAddressString;

    private String mailServicePasswordString;

    private final String host = "smtp.gmail.com";
    private final String port = "587";
    private final String auth = "true";
    private final String starttlsEnable = "true";
    private Session session;

    public MailService() {
        this.session = initSession();
    }

    public void setMailServiceAddressString(String mailServiceAddressString) {
        this.mailServiceAddressString = mailServiceAddressString;
    }

    public void setMailServicePasswordString(String mailServicePasswordString) {
        this.mailServicePasswordString = mailServicePasswordString;
    }

    private Session initSession() {
        logger.info("Initialize mail session");
        Properties props = new Properties();
        props.put("mail.smtp.host", this.host);
        props.put("mail.smtp.port", this.port);
        props.put("mail.smtp.auth", this.auth);
        props.put("mail.smtp.starttls.enable", this.starttlsEnable);

        // Get the Session object.
        return Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(MailService.this.mailServiceAddressString, MailService.this.mailServicePasswordString);
                    }
                });
    }

    public void sendMail(String to, String cc, String subject, String body) {

        logger.info("Sending new email message...");
        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(this.session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(this.mailServiceAddressString));

            logger.info("Sending message To [" + to + "]");

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Cc: header field of the header.
            if(null != cc) {
                logger.info("Sending message: Cc [" + cc + "]");
                message.setRecipients(Message.RecipientType.CC,
                        InternetAddress.parse(cc));
            }

            // Set Subject: header field
            logger.info("Sending message: subject [" + subject + "]");
            message.setSubject(subject);

            // Now set the actual message
            logger.info("Sending message: body [" + body + "]");
            message.setText(body);

            // Send message
            Transport.send(message);

            logger.info("Sent message successfully....");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void mainOffice(){
        final String username = "m.sorbara@lutech.it";
        final String password = "Gennaio.15";
        String from = "m.sorbara@lutech.it";
        String to = "sipestest@gmail.com";
        String host = "smtp.office365.com";
        String port = "587";
        String auth = "true";
        String starttlsEnable = "true";
        String subject = "SUBJECT";
        String text = "CIAO MAMMA";

        System.out.println("TLSEmail Start");

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", starttlsEnable);

        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try{
            MimeMessage msg = new MimeMessage(session);
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(from, "NoReply-JD"));

            msg.setReplyTo(InternetAddress.parse("no_reply@journaldev.com", false));

            msg.setSubject(subject, "UTF-8");

            msg.setText(text, "UTF-8");

            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));

            System.out.println("Message is ready");
            Transport.send(msg);

            System.out.println("EMail Sent Successfully!!");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new MailService().sendMail("m.sorbara@lutech.it", "a.sbarra@lutech.it", "Testing Subject", "Hello, this is sample for to check send "
                + "email using JavaMailAPI ");
    }
}
