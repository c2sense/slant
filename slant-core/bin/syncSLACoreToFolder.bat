REM Copy slacore project to $1 directory parameter
REM The slacore project is located in %ORIGIN%. Change the variable 
REM according to your location

SET SYNC_COMMAND="c:\Program Files (x86)\BlinkSync"\blinksync.exe
SET ORIGIN=c:/slant/sourceCode/trunk
SET DESTINATION=%1
SET CURRENTDIR=%CD%

IF not exist "%DESTINATION%" (mkdir "%DESTINATION%")
CD %ORIGIN%

copy configuration.properties.sample %DESTINATION%
copy pom.xml %DESTINATION%
copy README.md %DESTINATION%

%SYNC_COMMAND% -d "%ORIGIN%\docs" "%DESTINATION%\docs"
%SYNC_COMMAND% -d "%ORIGIN%\samples" "%DESTINATION%\samples"
%SYNC_COMMAND% -d "%ORIGIN%\bin" "%DESTINATION%\bin"

%SYNC_COMMAND% -d "%ORIGIN%\slant-enforcement\src" "%DESTINATION%\slant-enforcement\src"
copy "%ORIGIN%\slant-enforcement\pom.xml" "%DESTINATION%\slant-enforcement\"

IF not exist "%DESTINATION%\slant-personalization\src" (mkdir "%DESTINATION%\slant-personalization\src")
IF not exist "%DESTINATION%\slant-personalization\pom.xml" (copy "%ORIGIN%\slant-personalization\pom.xml" "%DESTINATION%\slant-personalization\")
IF not exist "%DESTINATION%\slant-personalization\src\main\resources" (mkdir "%DESTINATION%\slant-personalization\src\main\resources\")
IF not exist "%DESTINATION%\slant-personalization\src\main\resources\security-context.xml" (copy "%ORIGIN%\slant-personalization\src\main\resources\security-context.xml" "%DESTINATION%\slant-personalization\src\main\resources\")

%SYNC_COMMAND% -d "%ORIGIN%\slant-repository\src" "%DESTINATION%\slant-repository\src"
copy "%ORIGIN%\slant-repository\pom.xml" "%DESTINATION%\slant-repository\"

%SYNC_COMMAND% -d "%ORIGIN%\slant-tools\src" "%DESTINATION%\slant-tools\src"
copy "%ORIGIN%\slant-tools\pom.xml" "%DESTINATION%\slant-tools\"

%SYNC_COMMAND% -d "%ORIGIN%\slant-service\src" "%DESTINATION%\slant-service\src"
copy "%ORIGIN%\slant-service\pom.xml" "%DESTINATION%\slant-service\"

CD %CURRENTDIR%