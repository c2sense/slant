#!/usr/bin/env bash
if [ "$0" != "bin/dumpDatabase.sh" ]; then
	echo "Must be executed from project root"
	exit 1
fi
mysqldump -d -p -u slant slant > slant-repository/src/main/resources/sql/slant.sql
