#!/bin/bash

<<USAGE_EXAMPLE
export slant_host_port="C2sense-ie.ns0.it"
export slant_project_artifactId="slant-service"
export slant_tomcat_directory="\/opt\/tomcat"
export slant_db_username="slant"
export slant_db_password="_slant_"
export slant_db_name="slant"
export slant_db_host=192.168.205.36
export slant_db_port=3306
export slant_log_fullpathFilename="\/var\/log\/tomcat7\/slantfile.log"
export slant_log_thirdpartysw_fullpathFilename="\/var\/log\/tomcat7\/slantfullfile.log"
export slant_service_basicsecurity_user="user"
export slant_service_basicsecurity_password="secret"
export slant_enforcement_constraintEvaluator_class=it.lutech.c2sense.sla.monitoring.C2SenseConstraintEvaluator
export slant_enforcement_metricsRetriever_class=it.lutech.c2sense.sla.C2SenseSimpleMetricsRetriever
USAGE_EXAMPLE

cp slant-core/bin/configuration.properties slant-core/conf/env/configuration.properties

sed 's/{slant_host_port}/'$slant_host_port'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_project_artifactId}/'$slant_project_artifactId'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_tomcat_directory}/'$slant_tomcat_directory'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_db_username}/'$slant_db_username'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_db_password}/'$slant_db_password'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_db_name}/'$slant_db_name'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_db_host}/'$slant_db_host'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_db_port}/'$slant_db_port'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_log_fullpathFilename}/'$slant_log_fullpathFilename'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_log_thirdpartysw_fullpathFilename}/'$slant_log_thirdpartysw_fullpathFilename'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_service_basicsecurity_user}/'$slant_service_basicsecurity_user'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_service_basicsecurity_password}/'$slant_service_basicsecurity_password'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_enforcement_constraintEvaluator_class}/'$slant_enforcement_constraintEvaluator_class'/g' slant-core/conf/env/configuration.properties -i
sed 's/{slant_enforcement_metricsRetriever_class}/'$slant_enforcement_metricsRetriever_class'/g' slant-core/conf/env/configuration.properties -i

echo "Autoconfiguring executed successfully"

exit 0
