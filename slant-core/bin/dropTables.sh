DROP=$(mysql -p"_slant_" -u slant slant <<< "show tables" | grep -v "Tables" | sed -e's/\(.*\)/drop table \1; /')
SQL=$(echo "SET FOREIGN_KEY_CHECKS=0;" && echo $DROP)
echo "$SQL" | mysql -p -u slant slant
