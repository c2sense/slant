/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.sla;

import org.apache.commons.codec.binary.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class RESTInvoker {
    private final String baseUrl;
    private final String username;
    private final String password;

    public RESTInvoker(String baseUrl, String username, String password) {
        this.baseUrl = baseUrl;
        this.username = username;
        this.password = password;
    }

    public String callRESTProviders(){
        return getDataFromServer("providers/", null);
    }

    public String callRESTProviderRegister(String xml){
        return getDataFromServer("providers/", xml);
    }

    public String callRESTTemplatesRegister(String xml){
        return getDataFromServer("templates/", xml);
    }

    public String callRESTCreateAgreement(String xml){
        return getDataFromServer("templates/", xml);
    }

    private String getDataFromServer(String path, String input) {
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(this.baseUrl + path);
            URLConnection urlConnection = setUsernamePassword(url);
            if(input != null) {
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Method", "POST");
                urlConnection.setRequestProperty("Content-Type", "application/xml");

                OutputStream os = urlConnection.getOutputStream();
                os.write(input.getBytes());
                os.flush();
            } else {
                urlConnection.setRequestProperty("Method", "GET");
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            reader.close();

            return sb.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private URLConnection setUsernamePassword(URL url) throws IOException {
        URLConnection urlConnection = url.openConnection();
        String authString = this.username + ":" + this.password;
        String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
        urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
        return urlConnection;
    }

    public static void main(String[] args) {
        providers();
        //newProvider();
        //newTemplate();
    }

    private static void providers() {
        RESTInvoker restInvoker = new RESTInvoker("http://localhost:8080/slant-service/", "user", "password");
        String providers = restInvoker.callRESTProviders();
        System.out.println(providers);
    }

    private static void newProvider() {
        RESTInvoker restInvoker = new RESTInvoker("http://localhost:8080/slant-service/", "user", "password");
        String providers = restInvoker.callRESTProviders();
        System.out.println(providers);

        StringBuilder sb = new StringBuilder(4);
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<provider>\n");
        sb.append("    <name>provider01name</name>\n");
        sb.append("</provider>");

        String registeredProvider = restInvoker.callRESTProviderRegister(sb.toString());
        System.out.println(registeredProvider);
    }

    private static void newTemplate() {
        RESTInvoker restInvoker = new RESTInvoker("http://localhost:8080/slant-service/", "user", "password");
        //TODO Read a file from the file system is easier
        StringBuilder sb = new StringBuilder(4);
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<wsag:Template...");

        String registeredTemplate = restInvoker.callRESTTemplatesRegister(sb.toString());
        System.out.println(registeredTemplate);
    }

    private static void createAgreement() {
        RESTInvoker restInvoker = new RESTInvoker("http://localhost:8080/slant-service/", "user", "password");
        //TODO Read a file from the file system is easier
        StringBuilder sb = new StringBuilder(4);
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<wsag:Agreement...");

        String agreementCreated = restInvoker.callRESTCreateAgreement(sb.toString());
        System.out.println(agreementCreated);
    }

}
