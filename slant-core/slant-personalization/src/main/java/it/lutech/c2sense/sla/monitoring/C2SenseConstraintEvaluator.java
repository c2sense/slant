/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.sla.monitoring;

import eu.atos.sla.evaluation.constraint.IConstraintEvaluator;
import eu.atos.sla.evaluation.constraint.simple.SimpleConstraintEvaluator;
import eu.atos.sla.evaluation.constraint.simple.SimpleConstraintParser;
import eu.atos.sla.evaluation.constraint.simple.SimpleConstraintParser.SimpleConstraintElements;
import eu.atos.sla.monitoring.IMonitoringMetric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/* egarrido 
 * it is possible to configure the file in order to use eu.atos.sla.evaluation.constraint.simple.SimpleConstraintEvaluator
 * directly. We are preparing this just to test and decide if we need it.
 * */

public class C2SenseConstraintEvaluator implements IConstraintEvaluator {
	private static Logger logger = LoggerFactory.getLogger(C2SenseConstraintEvaluator.class);
	static SimpleConstraintParser parser = new SimpleConstraintParser();
	static SimpleConstraintEvaluator evaluator = new SimpleConstraintEvaluator();
	@Override
	public List<IMonitoringMetric> evaluate(String kpiName, String constraint,
			List<IMonitoringMetric> metrics) {
		logger.info("StartOf evaluate -  kpiName:"+kpiName+" - constraint:"+constraint+ "- metrics:"+metrics);
		evaluator.evaluate(kpiName, constraint, metrics);
		List<IMonitoringMetric> result = evaluator.evaluate(kpiName, constraint, metrics);
		logger.info("EndOf evaluate");
		return result;
	}

	@Override
	public String getConstraintVariable(String constraint) {
		String result = null;
		synchronized (parser){
			SimpleConstraintElements elems = parser.parse(constraint);
			result = elems.getLeft();
		}
		return result;
	}

}
