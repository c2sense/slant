/*
 * Copyright 2016 Lutech S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.lutech.c2sense.sla;

import eu.atos.sla.dao.IServiceReferenceDAO;
import eu.atos.sla.datamodel.IGuaranteeTerm;
import eu.atos.sla.datamodel.IServiceReference;
import eu.atos.sla.datamodel.IServiceVariable;
import eu.atos.sla.monitoring.IMetricsRetrieverV2;
import eu.atos.sla.monitoring.IMonitoringMetric;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.naming.ConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class C2SenseSimpleMetricsRetriever implements IMetricsRetrieverV2 {
    public static final String URL_MONITOR = "URL_Monitor";
    private static Logger logger = LoggerFactory.getLogger(C2SenseSimpleMetricsRetriever.class);

    //Prefix of the Guarantee Terms of the agreements
    private static String PREFIX_GT = "GT_";

    @Autowired
    private IServiceReferenceDAO serviceReferenceDAO;
    private String username = "user";
    private String password = "user";

    public String init(String agreementId) throws ConfigurationException {
        logger.info("Init class C2SenseSimpleMetricsRetriever for agreementId: " + agreementId);
        IServiceReference variableSet = this.serviceReferenceDAO.getByAgreementId(agreementId);
        if(variableSet != null) {
            for (IServiceVariable serviceVariable : variableSet.getServiceVariableSet()) {
                if (URL_MONITOR.equals(serviceVariable.getName())) {
                    return serviceVariable.getValue();
                }
            }
        }
        throw new ConfigurationException(URL_MONITOR + " not found for agreementId " + agreementId);
    }

    public String getURL(String url_) {
        URL obj;
        try {
            obj = new URL(url_);
            HttpURLConnection c = (HttpURLConnection) obj.openConnection();

            String authString = this.username + ":" + this.password;
            String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
            c.setRequestProperty("Authorization", "Basic " + authStringEnc);

            // optional default is GET
            c.setRequestMethod("GET");

            //add request header
            c.setRequestProperty("accept", "application/json");
            c.setConnectTimeout(50000);
            int responseCode = c.getResponseCode();

            logger.info("Response Code : " + responseCode);
            switch (c.getResponseCode()) {
                case 200:
                case 201:
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(c.getInputStream()));
                    String inputLine;
                    StringBuilder response = new StringBuilder(10);

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    return response.toString();
            }
        } catch (MalformedURLException ex) {
            logger.error("Errore", ex);
            return null;
        } catch (IOException ex) {
            logger.error("Errore", ex);
            return null;
        }
        return null;
    }

    @Override
    public List<IMonitoringMetric> getMetrics(String agreementId,
                                              String serviceScope, String variable, Date begin, Date end,
                                              int maxResults) {
        logger.info("Not used");
        return null;
    }

    @Override
    public Map<IGuaranteeTerm, List<IMonitoringMetric>> getMetrics(
            String agreementId, List<RetrievalItem> retrievalItems,
            int maxResults) {

        HashMap<IGuaranteeTerm, List<IMonitoringMetric>> result = new HashMap<IGuaranteeTerm, List<IMonitoringMetric>>();

        try {
            String monitorUrl = init(agreementId);
            JSONArray jsonArray = parseJson(this.getURL(monitorUrl));

            logger.info("StartOf getMetrics -  agreementId:" + agreementId + " maxResults:" + maxResults);
            //Hashtable<String, JSONObject> hastableWithJsonObjects = getListOfJSONObjectWithMeasures(retrievalItems, jsonArray);

            for (RetrievalItem ri : retrievalItems) {

                String metricVariable;
                //Normalize the metric name in case that this include the prefix
                if (ri.getVariable().indexOf(PREFIX_GT) == -1) {
                    metricVariable = ri.getVariable();
                } else {
                    metricVariable = ri.getVariable().substring(PREFIX_GT.length());
                }

                logger.info("Will retrieve for variable:" + metricVariable + " serviceName:" + ri.getGuaranteeTerm().getServiceName());
                //JSONObject jsonobject = hastableWithJsonObjects.get(ri.getGuaranteeTerm().getServiceName());
                WeatherMonitoringMetric weatherMonitoringMetric = parseOrionMetric(jsonArray, metricVariable);
                if (weatherMonitoringMetric != null) {
                    List<IMonitoringMetric> list = result.get(ri.getGuaranteeTerm());
                    if (list == null) {
                        list = new ArrayList<IMonitoringMetric>();
                        result.put(ri.getGuaranteeTerm(), list);
                    }
                    list.add(weatherMonitoringMetric);
                } else {
                    logger.info("No metric retrieved for variable:" + metricVariable + " serviceName:" + ri.getGuaranteeTerm().getServiceName());
                }
            }
        } catch (ConfigurationException ce){
            logger.error("ConfigurationException", ce);
        }
        logger.info("EndOf getMetrics -  agreementId:"+agreementId + " maxResults:"+maxResults );
        return result;
    }

    private JSONArray parseJson(String resp_) {
        JSONArray json = null;
        if (resp_==null) return null;
        try {
            json = (JSONArray) new JSONParser().parse(resp_);
        } catch (ParseException ex) {
            logger.error("Errore", ex);
        }
        return json;
    }

    // al metrics must have the same format!!!!
    private WeatherMonitoringMetric parseOrionMetric(JSONArray jsonarrayFromMonitor, String variable){
        WeatherMonitoringMetric result = null;
        if (jsonarrayFromMonitor!=null){
            for(Object measureObj: jsonarrayFromMonitor) {
                JSONObject measures = (JSONObject) measureObj;
                if (variable.equals(measures.get("metricKey"))) {
                    Date timestamp = null;
                    String value = (String) measures.get("date");
                    //the timestamp  will maintain always this format "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                    SimpleDateFormat dateFormat = new SimpleDateFormat(WeatherMonitoringMetric.DateFormat);
                    try {
                        timestamp = dateFormat.parse(value);
                    } catch (java.text.ParseException e) {
                        logger.error("ERROR during the parse data for the measure: " + e.getMessage());
                    }
                    Object measureValue = measures.get("metricValue");
                    logger.info("measure " + variable + " with value: " + measureValue + " will be added");
                    result = new WeatherMonitoringMetric(variable, measureValue.toString(), timestamp);
                }
            }
        }
        return result;
    }

}

