{
  "swagger": "2.0",
  "info": {
        "description": "This is a SLA Negotiation tool GUI.\n\n<b>List of operations for SLA management activities to a new service provider:</b>\n<ol>\n  <li>Section 1: Create Provider with a unique name. Output will be the provider uuid. Take note of it.</li>\n  <li>Section 2: Create template. In the output there are the uuid of the template. Take note of it.</li>\n  <li>Section 3: Create agreement. Usually insert the id of the associated template. Output will be the agreement uuid. Take note of it.</li>\n  <li>Section 4: An agreement had been part of the creation was created enforcement but you must start it.</li>\n  <li>Section 5: Read the violations found during the inspection activities</li>\n</ol>\n",
    "version": "1.0.0",
    "title": "Swagger SLA Negotiation Tool GUI",
    "contact": {},
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host": "${host}:${port}",
  "basePath": "/${project.artifactId}/rest",
  "tags": [
    {
      "name": "1:",
      "description": "Provider Section"
    },
    {
      "name": "2:",
      "description": "Template Section"
    },
    {
      "name": "3:",
      "description": "Agreements section"
    },
    {
      "name": "4:",
      "description": "Enforcement section"
    },
    {
      "name": "5:",
      "description": "Violation section"
    }
  ],
  "schemes": [
    "http"
  ],
  "paths": {
    "/agreements": {
      "get": {
        "tags": [
          "3:"
        ],
        "summary": "This operation allows to get all the registered agreements in the SLANT core.",
        "description": "This operation allows to get all the registered agreements in the SLANT core.",
        "operationId": "getAgreements",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "consumerId",
            "in": "query",
            "description": "uuid of the consumer",
            "required": false,
            "type": "string"
          },
          {
            "name": "providerId",
            "in": "query",
            "description": "uuid of the provider",
            "required": false,
            "type": "string"
          },
          {
            "name": "templateId",
            "in": "query",
            "description": "uuid of the template the agreement is based on",
            "required": false,
            "type": "string"
          },
          {
            "name": "active",
            "in": "query",
            "description": "If true, agreements currently enforced are returned",
            "required": false,
            "type": "boolean"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          }
        }
      },
      "post": {
        "tags": [
          "3:"
        ],
        "summary": "Allows to create a new agreement. It takes a XML body the description of the agreement, following the WS-Agreement schem",
        "description": "Allows to create a new agreement. It takes a XML body the description of the agreement, following the WS-Agreement schema. It might include a AgreementId or not. In case of not beeing included, a uuid will be assigned.",
        "operationId": "uploadAgreement",
        "consumes": [
          "application/xml"
        ],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": false,
            "schema": {
              "$ref": "#/definitions/XmlBody"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created",
            "schema": {
              "type": "string"
            }
          },
          "409": {
            "description": "Returned when the uuid or name already exists in the database.<br>Returned when the provider uuid specified in the agreement doesn't exist in the database.<br>Returned when the template uuid specified in the agreement doesn't exist in the database."
          },
          "500": {
            "description": "Returned when incorrect data has been suplied."
          }
        }
      }
    },
    "/agreements/active": {
      "get": {
        "tags": [
          "3:"
        ],
        "summary": "This operation allows to get all the active agreements in the SLANT core.",
        "description": "This operation allows to get all the active agreements in the SLANT core.",
        "operationId": "getAgreementsActive",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          }
        }
      }
    },
    "/agreements/{AgreementId}": {
      "get": {
        "tags": [
          "3:"
        ],
        "summary": "Returns agreement info",
        "description": "Returns agreement info",
        "operationId": "getAgreementsForAgreementId",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "AgreementId",
            "in": "path",
            "description": "Id of the agreement",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when the object is not in the database."
          }
        }
      }
    },
    "/agreements/{AgreementId}/context": {
      "get": {
        "tags": [
          "3:"
        ],
        "summary": "Only the context from the agreement identified by AgreementId is returned.",
        "description": "Only the context from the agreement identified by AgreementId is returned.",
        "operationId": "getAgreementsContextForAgreementId",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "AgreementId",
            "in": "path",
            "description": "Id of the agreement",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when the object is not in the database."
          },
          "500": {
            "description": "Returned when the data agreement was recorded incorrectly and the data cannot be supplied."
          }
        }
      }
    },
    "/agreements/{AgreementId}/guaranteestatus": {
      "get": {
        "tags": [
          "3:"
        ],
        "summary": "Gets the information of the status of the different Guarantee Terms of an agreement.<br>There are three available states",
        "description": "Gets the information of the status of the different Guarantee Terms of an agreement.<br>There are three available states: NON_DETERMINED, FULFILLED, VIOLATED.",
        "operationId": "getAgreementsGuaranteeStatusForAgreementId",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "AgreementId",
            "in": "path",
            "description": "Id of the agreement",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when the uuid doesn't exist in the database."
          }
        }
      }
    },
    "/enforcements/": {
      "get": {
        "tags": [
          "4:"
        ],
        "summary": "Returns all enforcements",
        "description": "Returns all enforcements",
        "operationId": "getAllEnforcements",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          }
        }
      }
    },
    "/enforcements/{AgreementId}": {
      "get": {
        "tags": [
          "4:"
        ],
        "summary": "Returns agreement info",
        "description": "Returns agreement info",
        "operationId": "getEnforcementsForAgreementId",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "AgreementId",
            "in": "path",
            "description": "Id of the agreement",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when the given uuid doesn't exist in the database."
          }
        }
      }
    },
    "/enforcements/{AgreementId}/start": {
      "put": {
        "tags": [
          "4:"
        ],
        "summary": "Start enforcement for agreement id",
        "description": "Start enforcement for agreement id",
        "operationId": "startenforcementForAgreementId",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "AgreementId",
            "in": "path",
            "description": "Id of the agreement",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          }
        }
      }
    },
    "/enforcements/{AgreementId}/stop": {
      "put": {
        "tags": [
          "4:"
        ],
        "summary": "Stop enforcement for agreement id",
        "description": "Stop enforcement for agreement id",
        "operationId": "stopenforcementForAgreementId",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "AgreementId",
            "in": "path",
            "description": "Id of the agreement",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          }
        }
      }
    },
    "/providers": {
      "get": {
        "tags": [
          "1:"
        ],
        "summary": "This operations allows to get all the registered providers in the SLANT core.",
        "description": "This operations allows to get all the registered providers in the SLANT core.",
        "operationId": "getProviders",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          }
        }
      },
      "post": {
        "tags": [
          "1:"
        ],
        "summary": "Allows to create a new provider. It takes a XML body containing the name of the provider.",
        "description": "Allows to create a new provider. It takes a XML body containing the name of the provider.",
        "operationId": "uploadProvider",
        "consumes": [
          "application/json",
          "application/xml"
        ],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": false,
            "schema": {
              "$ref": "#/definitions/XmlProvider"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created",
            "schema": {
              "type": "string"
            }
          },
          "409": {
            "description": "Returned when the uuid or name already exists in the database."
          }
        }
      }
    },
    "/providers/{uuid}": {
      "get": {
        "tags": [
          "1:"
        ],
        "summary": "This operations allows to get the registered provider in the SLANT core.",
        "description": "This operations allows to get the registered provider in the SLANT core.",
        "operationId": "getProvider",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "uuid",
            "in": "path",
            "description": "Id of the provider",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when the object is not in the database."
          }
        }
      },
      "delete": {
        "tags": [
          "1:"
        ],
        "summary": "This operations allows to delete a provider in the SLANT core.",
        "description": "This operations allows to delete a provider in the SLANT core.",
        "operationId": "deleteProviders",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "uuid",
            "in": "path",
            "description": "Id of the provider",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "204": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when the given uuid doesn't exist in the database."
          },
          "409": {
            "description": "Returned when the provider code is being used."
          }
        }
      }
    },
    "/templates": {
      "get": {
        "tags": [
          "2:"
        ],
        "summary": "This operations allows to get all the registered providers in the SLANT core.",
        "description": "This operations allows to get all the registered providers in the SLANT core.",
        "operationId": "getTemplates",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "serviceIds",
            "in": "query",
            "description": "String with comma separated values with the id's of service that is associated to the template.",
            "required": false,
            "type": "array",
            "items": {
              "type": "string"
            },
            "collectionFormat": "multi"
          },
          {
            "name": "providerId",
            "in": "query",
            "description": "Id of the provider that is offering the template.",
            "required": false,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          }
        }
      },
      "post": {
        "tags": [
          "2:"
        ],
        "summary": "Allows to create a new template. It takes a XML body the description of the template, following the WS-Agreement schema.",
        "description": "Allows to create a new template. It takes a XML body the description of the template, following the WS-Agreement schema. It might include a TemplateId or not. In case of not beeing included, a uuid will be assigned.<br><ul><li>TemplateId (optional, string) - The id of the template.</li><li>Name (string) - A human readable name for the agreement.</li><li>Context (object)<ul><ul><li>AgreementInitiator (string) - The name of the agreement initiator (in our case, an existing provider)</li><li>AgreementResponder (optional, string) - The name of the responder.</li><li>ServiceProvider (string) - The name of the service provider (an existing provider)</li><li>Service (string) - The service ID the agreement will monitor.</li><li>ExpirationTime (string) - The expiration date of the template (YYYY-MM-DDThh:mm:ss+hhmmformat)</li></ul></ul></li><li><p>Terms (object) - A set of terms.</p><ul><li>ServiceDescriptionTerm (object)<ul><li>Name (string) - The name of the service description term</li><li>ServiceName (string) - The service ID the services are based on.</li><li>Value (string) - Human readable description of the service description term.</li></ul></li><li>ServiceProperties (array[VariableSet]) - A list of variable sets.<ul><li>Name (string) - Name of the properties.</li><li>ServiceName (string) - Name of the service where the properties apply.</li><li>VariableSet (array[Variable]) - A list of variables.<ul><li>Variable (object)<ul><li>Name (string) - The name of the variable.</li></ul></li></ul></li></ul></li><li>GuaranteeTerm (array[ServiceLevelObjetive]) - A list of ServiceScope and ServiceLevelObjective.<ul><li>Name (string) - Descriptive name of the Guarantee Term.</li><li>ServiceScope (object)<ul><li>ServiceName (string) - name of the service.</li><li>Value (string) - Id of the scope of the service.</li></ul></li><li>ServiceLevelObjetive (object)<ul><li>KPITarget (object)<ul><li>KPIName (string) - Name of the KPI to be monitored.</li><li>CustomServiceLevel (string) - Custom values to be fulfilled by the monitored KPI.</li></ul></li></ul></li></ul></li></ul></li></ul>",
        "operationId": "uploadTemplate",
        "consumes": [
          "application/json",
          "application/xml"
        ],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": false,
            "schema": {
              "$ref": "#/definitions/XmlBody"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created"
          },
          "409": {
            "description": "Returned when the uuid or name already exists in the database.<br>Returned when the provider uuid specified in the template doesn't exist in the database."
          },
          "500": {
            "description": "Returned when incorrect data has been suplied."
          }
        }
      }
    },
    "/templates/{uuid}": {
      "get": {
        "tags": [
          "2:"
        ],
        "summary": "Returns template for template id",
        "description": "Returns template for template id",
        "operationId": "getTemplatesForUuid",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "uuid",
            "in": "path",
            "description": "Id of the template",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          }
        }
      },
      "put": {
        "tags": [
          "2:"
        ],
        "summary": "Updates the template identified by Uuid. The body might include a Uuid or not. In case of including a Uuid in the file, ",
        "description": "Updates the template identified by Uuid. The body might include a Uuid or not. In case of including a Uuid in the file, it must match with the one from the url.",
        "operationId": "updateTemplatesForUuid",
        "consumes": [
          "application/xml"
        ],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "uuid",
            "in": "path",
            "description": "Id of the template",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "body",
            "required": false,
            "schema": {
              "$ref": "#/definitions/XmlBody"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "409": {
            "description": "Returned when the uuid from the url doesn't match with the one from the file or when the system has already an agreement associated.<br>Returned when the template has agreements associated.<br>Returned when the provider doesn't exist."
          },
          "500": {
            "description": "Returned when incorrect data has been suplied."
          }
        }
      },
      "delete": {
        "tags": [
          "2:"
        ],
        "summary": "This operations allows to delete a provider in the SLANT core.",
        "description": "This operations allows to delete a provider in the SLANT core.",
        "operationId": "deleteTemplate",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "uuid",
            "in": "path",
            "description": "Id of the template",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "204": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when the given uuid doesn't exist in the database."
          },
          "409": {
            "description": "Returned when the provider code is being used."
          }
        }
      }
    },
    "/violations": {
      "get": {
        "tags": [
          "5:"
        ],
        "summary": "This operations allows to get all the violations in the SLANT core.",
        "description": "This operations allows to get all the violations in the SLANT core.",
        "operationId": "getViolations",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "agreementId",
            "in": "query",
            "description": "Id of the agreement",
            "required": false,
            "type": "string"
          },
          {
            "name": "guaranteeTerm",
            "in": "query",
            "description": "Name of the guaranteeTerm",
            "required": false,
            "type": "string"
          },
          {
            "name": "providerId",
            "in": "query",
            "description": "Id of the provider that is offering the template.",
            "required": false,
            "type": "string"
          },
          {
            "name": "begin",
            "in": "query",
            "description": "Lower limit of date of violations to search. Date format: yyyy-MM-dd'T'HH:mm:ss",
            "required": false,
            "type": "string"
          },
          {
            "name": "end",
            "in": "query",
            "description": "Upper limit of date of violations to search. Date format: yyyy-MM-dd'T'HH:mm:ss",
            "required": false,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when erroneous data is provided in the call."
          }
        }
      }
    },
    "/violations/{uuid}": {
      "get": {
        "tags": [
          "5:"
        ],
        "summary": "Retrieves information from a violation identified by the uuid.",
        "description": "Retrieves information from a violation identified by the uuid.",
        "operationId": "getViolationsForAgreementUuid",
        "consumes": [],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "uuid",
            "in": "path",
            "description": "Agreement uuid values that need to be considered for filter",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "string"
            }
          },
          "404": {
            "description": "Returned when erroneous data is provided in the call."
          }
        }
      }
    }
  },
  "definitions": {
    "XmlBody": {
      "type": "object"
    },
    "XmlProvider": {
      "type": "object",
      "required": [
        "name"
      ],
      "properties": {
        "name": {
          "type": "string",
          "example": "providerMax",
          "description": "Provider name"
        },
        "uuid": {
          "type": "string",
          "description": "custom provider identifier (deprecated)"
        }
      }
    }
  }
}