package eu.atos.sla.util;

import eu.atos.sla.datamodel.IAgreement;
import eu.atos.sla.datamodel.ICompensation.IPenalty;
import eu.atos.sla.datamodel.IEnforcementJob;
import eu.atos.sla.datamodel.IProvider;
import eu.atos.sla.datamodel.ITemplate;
import eu.atos.sla.datamodel.IViolation;
import eu.atos.sla.parser.data.EnforcementJob;
import eu.atos.sla.parser.data.Penalty;
import eu.atos.sla.parser.data.Provider;
import eu.atos.sla.parser.data.Violation;
import eu.atos.sla.parser.data.wsag.Agreement;
import eu.atos.sla.parser.data.wsag.Context;
import eu.atos.sla.parser.data.wsag.Template;

public interface IModelConverter {

	IAgreement getAgreementFromAgreementXML(Agreement agreementXML, String payload) throws ModelConversionException;

	ITemplate getTemplateFromTemplateXML(Template templateXML, String payload) throws ModelConversionException;

	IEnforcementJob getEnforcementJobFromEnforcementJobXML(EnforcementJob enforcementJobXML) throws ModelConversionException;

	Context getContextFromAgreement(IAgreement agreement) throws ModelConversionException;
	
	IProvider getProviderFromProviderXML(Provider providerXML);

	EnforcementJob getEnforcementJobXML(IEnforcementJob enforcementJob);

	Provider getProviderXML(IProvider provider);
	
	Violation getViolationXML(IViolation violation);
	
	Penalty getPenaltyXML(IPenalty penalty);

}