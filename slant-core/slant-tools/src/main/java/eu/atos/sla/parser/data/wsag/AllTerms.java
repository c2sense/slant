package eu.atos.sla.parser.data.wsag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "All")
public class AllTerms {

	@XmlElement(name = "ServiceDescriptionTerm")
	private ServiceDescriptionTerm serviceDescriptionTerm;
	@XmlElement(name = "ServiceReference")
	private List<ServiceReference> serviceReference;
	@XmlElement(name = "ServiceProperties")
	private List<ServiceProperties> serviceProperties;
	@XmlElement(name = "GuaranteeTerm")
	private List<GuaranteeTerm> guaranteeTerms;

	public AllTerms() {
	}

	public ServiceDescriptionTerm getServiceDescriptionTerm() {
		return this.serviceDescriptionTerm;
	}

	public void setServiceDescriptionTerm(
			ServiceDescriptionTerm serviceDescriptionTerm) {
		this.serviceDescriptionTerm = serviceDescriptionTerm;
	}

	public List<ServiceReference> getServiceReference() {
		return this.serviceReference;
	}

	public void setServiceReference(List<ServiceReference> serviceReference) {
		this.serviceReference = serviceReference;
	}

	public List<ServiceProperties> getServiceProperties() {
		return this.serviceProperties;
	}

	public void setServiceProperties(List<ServiceProperties> serviceProperties) {
		this.serviceProperties = serviceProperties;
	}

	public List<GuaranteeTerm> getGuaranteeTerms() {
		return this.guaranteeTerms;
	}

	public void setGuaranteeTerms(List<GuaranteeTerm> guaranteeTerms) {
		this.guaranteeTerms = guaranteeTerms;
	}

}
