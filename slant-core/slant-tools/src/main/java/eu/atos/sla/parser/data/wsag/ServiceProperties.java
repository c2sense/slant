package eu.atos.sla.parser.data.wsag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ServiceProperties")
public class ServiceProperties {

	@XmlAttribute(name = "Name")
	private String name;
	@XmlAttribute(name = "ServiceName")
	private String serviceName;
	@XmlElement(name = "VariableSet")
	private VariableSet variableSet;

	public ServiceProperties() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public VariableSet getVariableSet() {
		return this.variableSet;
	}

	public void setVariable(VariableSet variableSet) {
		this.variableSet = variableSet;
	}

}
