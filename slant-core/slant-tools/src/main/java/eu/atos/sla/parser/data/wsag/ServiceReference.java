package eu.atos.sla.parser.data.wsag;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ServiceReference")
public class ServiceReference {

	@XmlAttribute(name = "Name", required = true)
	private String name;

	@XmlAttribute(name = "ServiceName", required = true)
	private String serviceName;

	@XmlElement(name = "ServiceVariableSet")
	private ServiceVariableSet serviceVariableSet;

	public ServiceReference() {
	}

	/**
	 * Gets the value of the name property.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the value of the name property.
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the serviceName property.
	 * 
	 */
	public String getServiceName() {
		return this.serviceName;
	}

	/**
	 * Sets the value of the serviceName property.
	 */
	public void setServiceName(String value) {
		this.serviceName = value;
	}

	public ServiceVariableSet getServiceVariableSet() {
		return this.serviceVariableSet;
	}

	public void setServiceVariable(ServiceVariableSet serviceVariableSet) {
		this.serviceVariableSet = serviceVariableSet;
	}

}
