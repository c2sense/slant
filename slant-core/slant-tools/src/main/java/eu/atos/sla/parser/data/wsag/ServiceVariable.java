package eu.atos.sla.parser.data.wsag;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ServiceVariable")
public class ServiceVariable {

	@XmlAttribute(name = "Name")
	private String name;

	@XmlElement(name = "Value")
	private String value;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
