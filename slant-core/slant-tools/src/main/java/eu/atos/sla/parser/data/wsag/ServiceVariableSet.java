package eu.atos.sla.parser.data.wsag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ServiceVariableSet")
public class ServiceVariableSet {

	@XmlElement(name = "ServiceVariable")
	private List<ServiceVariable> serviceVariable;

	public List<ServiceVariable> getServiceVariable() {
		return this.serviceVariable;
	}

	public void setServiceVariable(List<ServiceVariable> serviceVariable) {
		this.serviceVariable = serviceVariable;
	}
	

}
