# SLANT  Installation Guide #

* [Requirements](#requirements)
* [Installation](#installation)
	* [Download the project](#download)
	* [Creating the mysql database](#database)
	* [Importing the code into eclipse](#eclipse)
* [Configuration](#configuration)
* [Running](#running)
* [Testing](#testing)


## Requirements ##

The requirements to install a working copy of the SLANT are:

* Oracle JDK >=1.7
* Database to install the database schema for the service: Mysql>=5.0
* Maven >= 3.0

## Installation ##

All commands shown here are ready to be executed from the root directory of the project (i.e., the one with this _README.md_ file) 

###1. Download the project ###

Clone the project using git from the [slant repository](https://bitbucket.org/Lutech-C2SENSE/slant.src)

	$ git clone https://bitbucket.org/Lutech-C2SENSE/slant.git

###2. Creating the mysql database ###

From mysql command tool, create a database (with a user with sufficient privileges, as root):

	$ mysql -p -u root 
	
	mysql> CREATE DATABASE slant;

Create a user:

	mysql> CREATE USER slant@localhost IDENTIFIED BY '_slant_';
	mysql> GRANT ALL PRIVILEGES ON slant.* TO slant@localhost; -- * optional WITH GRANT OPTION;

The slant webapp will create all the needed tables when loaded by first time.

	$ mvn test exec:java -f slant-repository/pom.xml

Another option to create the database is execute a sql file from the project root directory:

	$ bin/restoreDatabase.sh

The names used here are the default values of the slant. See [configuration](#configuration) to know how to change the values.

### 3. Importing the code into eclipse ###

The core of the Lutech SLANT has been developed using the Eclipse Java IDE, although others Java editors could be used, here we only provide the instructions about how to import the code into Eclipse.

The first step is to tell Maven to create the necessary Eclipse project files executing this:

	$ mvn eclipse:eclipse

The previous command is going to generate the eclipse project files: 
.settings, .classpath, and .project. Again, please never upload those files to the svn, it is going to deconfigure the eclipse of other developers (it is easy to fix, just an annoying waste of time).

After it, from your eclipse you can import the project. Go to "import project from file", go to the trunk folder, and you should see the "SLANT" project ready to be imported in your Eclipse. 

## Configuration ##

The project is made up of five main modules:

- SLANT Repository
- SLANT Enforcement
- SLANT Service
- SLANT Tools
- SLANT Personalization

A _dev.properties_ and a _test.properties_ are placed in the _slant_core/conf/env_ directory. Suitably customize these environments for the respective files.

Several parameters can be configured through this files.

1. `host_port` and `project_artifactId`: allows to configure the swagger.json for swagger applications like [SLANT GUI project](https://bitbucket.org/Lutech-C2SENSE/slant-gui),
1. `tomcat.directory`: when building, war will be automatically copied to this directory; useful just for development environment,
1. `db.\*`: allows to configure the database username, password and name in case it has been changed from the proposed one in the section [Creating the mysql database](#database). It can be selected if queries from hibernate must be shown or not. These parameters can be overriden at deployment time through the use of environment variables (see section [Running](#running)),
1. `log.\*`: allows to configure the log files to be generated and the level of information,
1. `enforcement.\*`: several parameters from the enforcement can be customized,
1. `service.basicsecurity.\*`: if basic security is enabled, these parameters can be used to set the user name and password to access to the rest services.
1. `parser.*`: different parsers can be implemented for the agreement and template. By default, wsag standard parsers are have been implemented and configured in the file. Also dateformat can be configured.

Another way for creating in an automated manner the configurations properties file is to set some global variables and run the bin/autoconfigure.sh script.
It is a simple script that takes the values from the exported OS variables, substitutes the proper values in the `slant-core/bin/configuration.properties` file and creates the `slant-core/conf/env/configuration.properties` file with the declared values. This allows, for example, to automatically configure and deploy the SLANT in continuous integration systems like Jenkins.

More concretely, the variables to be set are:

    slant_host_port
    slant_project_artifactId
    slant_tomcat_directory
    slant_db_username
    slant_db_password
    slant_db_name
    slant_db_host
    slant_db_port
    slant_log_fullpathFilename
    slant_log_thirdpartysw_fullpathFilename
    slant_service_basicsecurity_user
    slant_service_basicsecurity_password
    slant_enforcement_constraintEvaluator_class
    slant_enforcement_metricsRetriever_class

As an example, some of the values we set to get the preview version of the Slant to be configured automatcally by Jenkins are:

    export slant_host_port="C2sense-ie.ns0.it"
           (or export slant_host_port="C2sense-ie.ns0.it:8080")
    export slant_project_artifactId="slant-service"
    export slant_tomcat_directory="\/opt\/tomcat"
    export slant_db_username="slant"
    export slant_db_password="_slant_"
    export slant_db_name="slant"
    export slant_db_host=mysql
    export slant_db_port=3306
    export slant_log_fullpathFilename="\/var\/log\/tomcat7\/slantfile.log"
    export slant_log_thirdpartysw_fullpathFilename="\/var\/log\/tomcat7\/slantfullfile.log"
    export slant_service_basicsecurity_user="user"
    export slant_service_basicsecurity_password="secret"
    export slant_enforcement_constraintEvaluator_class=it.lutech.c2sense.sla.monitoring.C2SenseConstraintEvaluator
    export slant_enforcement_metricsRetriever_class=it.lutech.c2sense.sla.C2SenseSimpleMetricsRetriever
    
If you're creating the database using the command _mvn test exec:java -f slant-repository/pom.xml_ please make sure that you configure properly slant-repository\src\main\resources\META-INF\persistence.xml. Make sure you're setting the username, password and connection url with the proper parameters.

	<property name="hibernate.connection.username" value="slant" />
	<property name="hibernate.connection.password" value="_slant_" />
	<property name="hibernate.connection.url" value="jdbc:mysql://mysql:3306/slant" />

If you need a new environment, after the creation of a suitable `<target>.properties` file (for a `<target>` environment), you need to create a new maven profile in the _slant-core/parent.pom.xml_ file like _dev_ end _test_. 

## Compiling ##

For a correct packaging it's mandatory choose a profile for inject the parameters accordingly to the target environment.
For exemple for create a package to the test environment run the command: 

	$  mvn clean install -Penv-test
	
If you want to skip tests:
	
	$ mvn clean install -Penv-test -Dmaven.test.skip=true
	
The result of the command is a war in _slant-core/slant-service/target_. The war is also copied to the directory pointed by _tomcat.directory_ propertiy specified in the _test.properties_ file.

## Running ##

If the war was successfully copied to _tomcat.directory_, then start your tomcat to run the server.

Some configuration parameters can be overriden using environment variables or jdk variables (over the env properties). The list of parameters overridable is:

* `DB_DRIVER`; default value is `com.mysql.jdbc.Driver`
* `DB_URL`; default value is `jdbc:mysql://${db.host}:${db.port}/${db.name}`
* `DB_USERNAME`; default value is `${db.username}`
* `DB_PASSWORD`; default value is `${db.password}`
* `DB_SHOWSQL`; default value is `${db.showSQL}`
* `BROOKLYN_URL`; default value is `${brooklyn.url}`

F.e., to use a different database configuration:

	$ export DB_URL=jdbc:mysql://mysql:3306/slant
	$ export DB_USERNAME=slant
	$ export DB_PASSWORD=<secret>

## Testing ##

Check that everything is working performing the following HTTP call:

	$ curl http://C2sense-ie.ns0.it/slant-service/providers

The actual address depends on the tomcat configuration.

Time to check the User Guide!
